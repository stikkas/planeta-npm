/**
 * This is only for local test
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { PlHeaderModule }  from 'planeta-header'

@Component({
  selector: 'app',
  template: `<pl-header></pl-header>`
})
class AppComponent {}

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [ AppComponent ],
  imports: [ BrowserModule, PlHeaderModule ]
})
class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule);
