import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlHeaderComponent } from './pl.header.component';
import { PlAuthFormComponent } from './auth-form/pl.auth.form.component';

export * from './pl.header.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PlHeaderComponent,
    PlAuthFormComponent
  ],
  exports: [
    PlHeaderComponent
  ]
})

export class PlHeaderModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PlHeaderModule
    };
  }
}
