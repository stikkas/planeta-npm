import {Component} from '@angular/core';

@Component({
    selector: 'pl-header',
    templateUrl: './pl.header.component.html',
    styleUrls: [
        './less/common.less'
    ]
})

export class PlHeaderComponent {

    constructor() {
    }

}
